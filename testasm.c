#include <string.h>

extern void payload(void*);
extern void payload_end();

void do_call(void* ptr_) {
	size_t ptr = (size_t) ptr_;
	size_t sz = (size_t) payload_end;
	sz -= (size_t) payload;
	char buff[sz];
	memcpy(buff, payload, sz);
	//payload(buff);
	((void (*)(void))buff)();

	//overrun the return address in the call to from main
	//for (size_t i = ((size_t)buff)+sz; i < ptr; i += sizeof(void*)) {
	//	void** p = (void*)i;
	//	*p = buff;
	//}
}

int main() {
	int x = 0;
	do_call(&x);
}
