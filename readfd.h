#ifndef _READ_FD_H
#define _READ_FD_H
extern void* readfd(int, size_t*);
#endif
