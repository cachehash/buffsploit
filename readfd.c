#include <stdlib.h>
#include <unistd.h>

void* readfd(int fd, size_t *size) {
	const int chunks = 512;
	char *buff = malloc(chunks);
	size_t idx = 0;
	for(;;) {
		int red = read(fd, buff + idx, sizeof(char));
		if (red == 0) {
			break;
		}
		idx += red;
		buff = realloc(buff, idx+chunks);
	}
	if (size) {
		*size = idx;
	}
	return buff;
}
