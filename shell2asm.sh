#!/bin/bash
arch=$2
case $arch in
i386)
	wordsize=4
	dump_asm() {
		tac | rev | xxd -c $wordsize -ps | sed -e 's/.*/pushl\t$0x&/'
	}
;;
amd64|x86_64)
	wordsize=8
	dump_asm() {
		tac | rev | xxd -c $wordsize -ps | sed -e 's/.*/movq\t$0x&, %rax\npushq\t%rax/'
	}
;;
mips64-build|mips-build)
	wordsize=4
	dump_asm() {
		tac | rev | xxd -c 1 -ps | paste - - - - | awk '
		{lines++}
		{
			#Put value in register
			print "lui\t$s8, 0x"$4$3
			print "ori\t$s8, $s8, 0x"$2$1
			#push register on stack
			print "sw\t$s8, OFFSET-"4*lines"($sp)"
		}
		4*lines >= 0xF8 {
			#store offset into stackpointer
			print "daddiu $sp, $sp, -"4*lines
			lines=0
		}
		END {
			if (lines != 0) {
				print "daddiu $sp, $sp, -"4*lines
			}
		}'
	}
;;
mips|mips64)
	wordsize=4
	dump_asm() {
		#Since arm has pc-relative, just put the script
		#on the stack, calculate its address from PC, and 
		#branch over it
		xxd -c 1 -ps | paste - - - - | awk ' { print "\t.word\t0x"$1$2$3$4 }'
	}
;;
arm|aarch64|riscv64)
	wordsize=4
	dump_asm() {
		#Since arm has pc-relative, just put the script
		#on the stack, calculate its address from PC, and 
		#branch over it
		xxd -c 1 -ps | paste - - - - | awk ' { print "\t.word\t0x"$4$3$2$1 }'
	}
;;
esac
byte=`wc -c "$1" | awk '{print $1}'`

#The number of bytes paste a word size we go
n=$((byte % wordsize))
#Calculate the number of bytes needed to reach a wordsize
n=$((wordsize - n))
{
	cat < "$1"
	printf %"$n"s ''
} | dump_asm
