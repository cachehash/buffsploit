//FIXME this is not needed
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "readfd.h"

#include <netdb.h>
#include <netinet/in.h>

#include <sys/personality.h>

#ifndef BUFF_SIZE
#define BUFF_SIZE 270
#endif
int bof(char *str) {
	char buffer[BUFF_SIZE];

	/* The following statement has a buffer overflow problem */ 
	strcpy(buffer, str);

	return 1;
}
//#define bof_wrap bof

int bof_wrap(char *str) {
	return bof(str)+1;
}

void usage(int status) {
	exit(status);
}
int main(int argc, char **argv) {
	int port = 0;
	int net = 0;
	int fd;
	int opt;
	while ((opt = getopt(argc, argv, "rl:")) != -1) {
		switch(opt) {
			case 'l':
				port = strtoul(optarg, NULL, 0);
				net = 1;
			break;
			case 'r':
				if (!getenv("_NO_ASLR_")) {
					setenv("_NO_ASLR_", "1", 1);
					personality(ADDR_NO_RANDOMIZE);
					execvp(argv[0], argv);
				}
			break;
		}
	}
	char* file = NULL;
	for (int i = optind; i < argc; i++) {
		if (file != NULL) {
			fprintf(stderr, "ERROR: can only specify 1 file to read\n");
			return 1;
		}
		file = argv[i];
	}
	if (file == NULL) {
		file = "buffer";
	} else if (net) {
		fprintf(stderr, "ERROR: can't specify a file to read with -l\n");
		return 1;
	}
	if (!net) {
		fd = open(file, O_RDONLY);
		if (fd < 0) {
			fprintf(stderr, "ERROR: can't open `%s' for read.\n", file);
			return 1;
		}
	} else {
		int sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 1) {
			fprintf(stderr, "ERROR: failed to open socket.\n");
			return 1;
		}
		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = htons(port);
		//I don't like using sockaddr_in instead of sockaddr...
		if (bind(sockfd, (void*) &addr, sizeof(addr)) < 0) {
			return 1;
		}
		listen(sockfd,1);
		fd = accept(sockfd, NULL, NULL);
		if (fd < 0) {
			return 1;
		}
	}
	
	char *str = readfd(fd, NULL);
	close(fd);
	bof_wrap(str);

	free(str);
	printf("Returned Properly\n");
	return 1;
}
