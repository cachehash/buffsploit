set logging off
set logging file gdb.output
#Break on the vunlerable function
b bof
#Run the program reading from the file /dev/null
if $native
	r
else
	c
end

set logging on
p (void*)buffer
