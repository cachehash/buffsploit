#include <sys/personality.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char ** argv) {
	personality(ADDR_NO_RANDOMIZE);
	execvp(argv[1], argv+1);
	fprintf(stderr, "ERROR: couldn't exec %s!\n", argv[1]);
}
