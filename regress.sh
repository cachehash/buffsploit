
unset tmp
trap 'rm -rf $tmp' EXIT
tmp=`mktemp -d`

arches='x86_64 i386 aarch64 arm mips mips64 riscv64'
for arch in $arches; do
cat <<EOF > payload.sh
echo 'Successfully ran contents of payload.sh from $arch!' | grep --color=always '.*'
touch '$tmp/$arch'
EOF
	make clean
	if [ $arch == `uname -m` ] ; then
		make
	else
		make ARCH=$arch
	fi
	echo "Testing arch $arch..." | grep --color=auto '.*'
	file vuln
	./vuln -r
done
rm payload.sh

echo
echo Summary:
fail=0
for arch in $arches; do
	fmt='%-12s%s\n'
	if [ -e "$tmp/$arch" ] ; then
		printf "$fmt" "$arch" 'test passed'
	else
		printf "$fmt" "$arch" 'test failed!' | grep --color=auto -e '.*'
		fail=1
	fi
done
exit $fail
