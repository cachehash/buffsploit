source common.gdb
set logging off

#in ARM gcc stores the return address before the buffer on the stack...
#finish this function, then finish the next so the return address is 
#stored in lr. Then search the stack for that return address
finish
finish
set logging on
find $sp-50*sizeof(void*), +50*sizeof(void*), $pc
