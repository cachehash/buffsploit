if grep -qPae '\x00' "$1" ; then
	echo "ERROR: \`$1' contains null!" | grep --color=auto -e '.*' >&2
	exit 1
fi
