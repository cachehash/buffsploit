source common.gdb
set logging off

#Proceed until we return from function
finish
#log search results
set logging on
#search for the return address in the ra register on the stack
find $sp-50*sizeof(void*), +50*sizeof(void*), $ra
