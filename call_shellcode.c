/* call_shellcode.c  */

/*A program that creates a file containing code for launching shell*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char **argv) {
	char buf[8192];
	FILE *input = fopen(argv[1], "r");
	fread(buf, 8192, 1, input);
	((void(*)())buf)();
} 
